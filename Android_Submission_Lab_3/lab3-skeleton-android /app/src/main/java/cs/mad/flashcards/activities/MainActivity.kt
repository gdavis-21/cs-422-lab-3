package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*
            connect to views using findViewById
            setup views here - recyclerview, button
            don't forget to notify the adapter if the data set is changed
         */
        val recycler = findViewById<RecyclerView>(R.id.recylerViewMain)
        var sets = FlashcardSet.getHardcodedFlashcardSets()
        val adapter = FlashcardSetAdapter(sets)
        recycler.adapter = adapter

        val button = findViewById<Button>(R.id.addFlashcardSet)
        button.setOnClickListener() {
            adapter.addFlashcardSet()
            adapter.notifyDataSetChanged()
        }
    }
}