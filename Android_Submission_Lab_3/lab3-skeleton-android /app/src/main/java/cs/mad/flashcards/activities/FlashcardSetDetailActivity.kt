package cs.mad.flashcards.activities

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard

class FlashcardSetDetailActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)
        val recycler = findViewById<RecyclerView>(R.id.recylerFlashcardSet)
        val adapter = FlashcardAdapter(Flashcard.getHardcodedFlashcards())
        recycler.adapter = adapter

        val button = findViewById<Button>(R.id.addFlashcard)
        button.setOnClickListener() {
            adapter.addFlashcard()
            adapter.notifyDataSetChanged()
        }
    }
}