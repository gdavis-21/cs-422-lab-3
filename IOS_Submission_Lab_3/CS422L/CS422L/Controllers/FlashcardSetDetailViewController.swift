//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Grant Davis on 2/3/22.
//

import Foundation
import UIKit

class FlashcardSetDetailViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var sets: [Flashcard] = Flashcard.getHardCodedFlashcards()
    
    @IBOutlet var tableView: UITableView!
    @IBAction func buttonAdd(_ sender: Any) {
        var flashcard = Flashcard()
        flashcard.term = "Term " + String(sets.count + 1)
        sets.insert(flashcard, at: sets.count)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //connect hard coded collection to sets
        tableView.delegate = self
        tableView.dataSource = self
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return number of items
        return sets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetCell", for: indexPath) as! FlashcardTableViewCell
        //setup cell display here
        cell.label.text = sets[indexPath.row].term
        return cell
    }
    
}
