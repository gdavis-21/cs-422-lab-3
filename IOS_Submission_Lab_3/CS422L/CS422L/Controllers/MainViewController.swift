//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    var sets: [FlashcardSet] = FlashcardSet.getHardCodedCollection()
    
    @IBOutlet var collectionView: UICollectionView!
    
    @IBAction func buttonAdd(_ sender: Any) {
        var flashcard = FlashcardSet()
        flashcard.title = "Term " + String(sets.count + 1)
        sets.insert(flashcard, at: sets.count)
        collectionView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //connect hard coded collection to sets
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return number of items
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionViewCell
        //setup cell display here
        if cell.label.text != nil {
            cell.label.text = sets[indexPath.row].title
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        //performSegue(withIdentifier: "GoToDetail", sender: self)
        performSegue(withIdentifier: "segueToFlashcards", sender: self)
    }
}

