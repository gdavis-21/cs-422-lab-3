Android

Target Device: Pixel 4 XL, API 30

    Display the hardcoded list of FlashcardSets from last week in MainActivity using a RecyclerView.
        Display as a grid
    Display the hardcoded list of Flashcards from last week in a new Activity called FlashcardSetDetailActivity, also using a RecyclerView.
        Display as a list
    Clicking a FlashcardSet item should launch FlashcardSetDetailActivity.
        viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
    In FlashcardSetDetailActivity above the RecyclerView, add a visual area to display a title, a delete button, a study button, and an add button.
        The add button should add a new Flashcard to the collection and reflect the change on the screen
        Will need to make use of the adapter's notify* functions
    Add a button to MainActivity that adds a new FlashcardSet item to the collection and reflects the change on the screen.
        Does not have to be in the app bar as shown above

iOS

Target Device: iPhone 11 Pro Max, iOS 14.4

    Rename ViewController to MainViewController.
        Remember to do this in both the class itself and in the project's file structure
    Display the hardcoded list of FlashcardSets from last week in MainViewController using a UICollectionView.
        Display as a grid
    Display the hardcoded list of Flashcards from last week in a new ViewController called FlashcardSetDetailViewController, using a UITableView.
        Display as a list
    Clicking a FlashcardSet item should launch FlashcardSetDetailViewController.
        Using a Segue in storyboard: performSegue(withIdentifier: "Your Segue Identifier You Set In StoryBoard", sender: self)
    In FlashcardSetDetailViewController above the UITableView, add a visual area to display a title, a delete button, a study button, and an add button.
        The add button should add a new Flashcard to the collection and reflect the change on the screen
    Add a button to MainViewController that adds a new FlashcardSet item to the collection and reflects the change on the screen.
        Does not have to be in the app bar as shown above

 

Bonus - Extra 5 points (2.5 points for iOS, 2.5 points for Android)

    Make items in Flashcard list delete on Swipe

 

Submitting

    Ensure the apps run on the designated emulator/simulator: Pixel 4 XL, API 30 and iPhone 11 Pro Max
    Submit a link to the specific commits on Bitbucket (or another Git repository)
        Your submission should have two links, one for iOS and one for Android
        To get a link for the specific commit in Bitbucket
            Go to the commit tab for the repository
            Click the unique alphanumeric code for the commit you want to submit
            On the commit details page, right-click the "View source" button and select "Copy Link Address"

